# usage example

```python
import json
import JsonEval as je

# add definitions from schema-file
scheme = je.Scheme()
with open("schema.json") as fp:
    schema.Update(json.load(fp))

# unpack using the schema
with open("example.json") as fp:
    jdata = json.load(fp
unpacked, err = scheme.Eval(jdata, "person")
```

# scheme file

A scheme-file is a JSON file (refer to `scheme.json` for a example) defining
a set of composite datatypes.

Each datatypes name has to be unique to the schema, and may not be any of
`string, number, bool, null, list, object, any`

scheme files can be loaded into a scheme directly by passing their json repr
to the scheme constructor. A scheme can be updated with Scheme.Update(repr)
(the same restrictions as above apply).


# return values

`Scheme.Eval(data, typeName)` returns a named-tuple if the evaluated base is
a *defined-object* (like `person` in `scheme.json`) or a *typed-list* (like
`t_boxlist`).

If the evaluated base is a base type, the return value is the default
representation of that type.

If the unpacking failed, the second return-value `err != None`


# types

The available base types are:
`string, number, bool, null, list, object, any`

`object` and `list` will evaluate against any {} or [] base object and return it unchanged.

`any` will evaluate against everything and return it unchanged.

Composite types that can be defined are *defined-objects* and *typed-lists*.

