# JsonEval provides facilities to define and evaluate JSON-schemes against
# input data. Evaluation ensures compliance with a scheme and makes the
# data available in a more convenient format (namedtuples)

from collections import namedtuple


# A Scheme is a collection of defined datatypes against which an input
# datatype can be evaluated. Datatypes may be composed of other datatypes
# defined in the same scheme
# Refer to the README.md and schema.json for a detailed explanation
class Scheme:
    def __init__(self, definition=None):
        self.types = {}
        self.types.update(basetypes)
        if definition:
            self.Update(definition)

    # Update adds datatype definitions to an existing scheme
    # Attempting to redefine an existing or base-datatype  will raise
    # an Exception
    def Update(self, definition):
        for typeName, typeDefinition in definition.items():
            if typeName in self.types:
                raise ValueError(f"duplicate type: {typeName}")
            if isinstance(typeDefinition, dict):
                self.types[typeName] = datatypeObject(typeDefinition)
            elif isinstance(typeDefinition, list):
                self.types[typeName] = datatypeList(typeDefinition)
            else:
                raise ValueError(f"bad type definition: '{typeDefinition}'")

    # Eval evaluates input data against a datatype in the scheme
    # par:  data: JSON, arbitrary JSON compatible data-structure
    #             usually an output of json.load()
    #       typeName: string, the name of a type in the scheme against
    #                 which `data` is to be evaluated. Can be a defined
    #                 composite type as well as one of the base types:
    #                 string, number, bool, null, list, object, any
    # ret:  nt: namedtuple|dict|list, type depending on evaluated type
    #           refer to `README.md` for details
    def Eval(self, data, typeName):
        return self.types[typeName].eval(self, data)


class datatypeObject:
    def __init__(self, definition):
        # fields: {name: type}
        self.fields = {}
        for fieldName, fieldDef in definition.items():
            self.fields[fieldName] = fieldDef["type"]

    def eval(self, scheme, data):
        # check for dict
        if not isinstance(data, dict):
            return None, f"bad type: expected object got '{data}'"
        # for building namedtuple
        keys = []
        vals = []
        for fieldName, fieldType in self.fields.items():
            if fieldName not in data:
                return None, f"missing field: {fieldName} not in '{data}'"
            val, err = scheme.types[fieldType].eval(scheme, data[fieldName])
            if err != None:
                return None, err
            keys.append(fieldName)
            vals.append(val)
        # mk & return namedtuple
        nt = namedtuple("nt", keys)
        return nt(*vals), None


class datatypeList:
    def __init__(self, definition):
        self.elemType = definition[0]

    def eval(self, scheme, data):
        l = []
        for elem in data:
            val, err = scheme.types[self.elemType].eval(scheme, elem)
            if err != None:
                return None, err
            l.append(val)
        return l, None

class datatypeBase:
    def __init__(self, typeName):
        self.typeName = typeName
        self.type = {
            "string": str,
            "number": (int, float),
            "bool": bool,
            "null": None,
            "object": dict,
            "list": list,
            "any": object
        }[typeName]
    def eval(self, scheme, data):

        if isinstance(data, self.type):
            return data, None
        return None, f"bad type: expected {self.typeName} got '{data}'"


basetypes = {
"string": datatypeBase("string"),
"number": datatypeBase("number"),
"bool": datatypeBase("bool"),
"null": datatypeBase("null"),
"list": datatypeBase("list"),
"object": datatypeBase("object"),
"any": datatypeBase("any"),
}

